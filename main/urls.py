from django.urls import path, include
from .views import *
from blog.views import blog_fa

urlpatterns = [
    path('en', mainen),
    path('fa', mainfa),
    path('blog', blog_fa),
]
