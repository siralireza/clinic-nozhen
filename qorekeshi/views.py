from django.contrib import messages
from django.shortcuts import render
from django.views import View

from .models import *
from random import randint
import datetime


def getgift(request):
    return render(request, 'gift.html')


class qorekeshi(View):
    def get(self, request):
        items = [' ١٠٠درصد؜ تخفيف ميكرودرم',
                 '١٠٠درصد؜ تخفيف هيدرومي و آبرساني پوست',
                 '١٠٠درصد تخفيف فول بادي كاربردي',
                 '٥٠درصد تخفيف ليزر رفع موهاي زائد',
                 '١٠٠درصد؜ تخفيف آراِف و ليفت صورت',
                 '٥٠درصد تخفيف كرايو ليپوليز',
                 ]
        if request.GET:
            form = gitfsForm(request.GET)
            if form.is_valid():
                if gifts.objects.filter(fullname=form.cleaned_data['fullname']).exists():
                    last_month = datetime.datetime.now() - datetime.timedelta(days=30)
                    if gifts.objects.get(
                            fullname=form.cleaned_data['fullname']).second_gift is not '' and not gifts.objects.filter(
                        fullname=form.cleaned_data['fullname'], created_at__lt=last_month).exists():
                        messages.add_message(request, messages.ERROR,
                                             'شما دو بار در این ماه  جایزه دریافت کرده اید لطفا ماه بعد تلاش نمایید.')
                        return render(request, 'gift.html')
                    if gifts.objects.filter(
                            fullname=form.cleaned_data['fullname'], created_at__lt=last_month).exists():
                        obj = gifts.objects.filter(fullname=form.cleaned_data['fullname']).first()
                        rand = randint(0, 4)
                        obj.first_gift = items[rand]
                        obj.second_gift = ''
                        obj.created_at = datetime.datetime.now()
                        obj.save()
                    else:
                        obj = gifts.objects.filter(fullname=form.cleaned_data['fullname']).first()
                        items.remove(obj.first_gift)
                        rand = randint(0, 4)
                        obj.second_gift = items[rand]
                        obj.created_at = datetime.datetime.now()
                        obj.save()
                else:
                    gift = form.save(commit=False)
                    rand = randint(0, 5)
                    gift.first_gift = items[rand]
                    gift.created_at = datetime.datetime.now()
                    gift.save()
                return render(request, 'gift.html', {'gift': items[rand], 'form': form})
            return render(request, 'gift-form.html', {'form': form})
        return render(request, 'gift-form.html')

    def post(self, request):
        items = [' ١٠٠٪؜ تخفيف ميكرودرم',
                 '١٠٠٪؜ تخفيف هيدرومي و آبرساني پوست',
                 '١٠٠٪؜ تخفيف فول بادي كاربردي',
                 '٥٠٪ تخفيف ليزر رفع موهاي زائد',
                 '١٠٠٪؜ تخفيف rf و ليفت صورت',
                 '٥٠٪؜ تخفيف كرايو ليپوليز',
                 ]

        form = gitfsForm(request.POST)
        if form.is_valid():
            if gifts.objects.filter(fullname=form.cleaned_data['fullname']).exists():
                last_month = datetime.datetime.now() - datetime.timedelta(days=30)
                if gifts.objects.get(
                        fullname=form.cleaned_data['fullname']).second_gift is not '' and not gifts.objects.filter(
                    fullname=form.cleaned_data['fullname'], created_at__lt=last_month).exists():
                    messages.add_message(request, messages.ERROR,
                                         'شما دو بار در این ماه  جایزه دریافت کرده اید لطفا ماه بعد تلاش نمایید.')
                    return render(request, 'gift.html')
                if gifts.objects.filter(
                        fullname=form.cleaned_data['fullname'], created_at__lt=last_month).exists():
                    obj = gifts.objects.filter(fullname=form.cleaned_data['fullname']).first()
                    rand = randint(0, 4)
                    obj.first_gift = items[rand]
                    obj.second_gift = ''
                    obj.created_at = datetime.datetime.now()
                    obj.save()
                else:
                    obj = gifts.objects.filter(fullname=form.cleaned_data['fullname']).first()
                    items.remove(obj.first_gift)
                    rand = randint(0, 4)
                    obj.second_gift = items[rand]
                    obj.created_at = datetime.datetime.now()
                    obj.save()
            else:
                gift = form.save(commit=False)
                rand = randint(0, 5)
                gift.first_gift = items[rand]
                gift.created_at = datetime.datetime.now()
                gift.save()
            return render(request, 'gift.html', {'gift': items[rand], 'form': form})
        return render(request, 'gift-form.html', {'form': form})
