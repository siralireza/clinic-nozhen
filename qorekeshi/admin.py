from django.contrib import admin

from qorekeshi.models import gifts


@admin.register(gifts)
class giftsAdmin(admin.ModelAdmin):
    list_display = ['id', 'fullname', 'phone',
                    'sex', 'first_gift', 'second_gift']
