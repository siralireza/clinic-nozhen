from django.db import models
from django.forms import ModelForm, forms


class gifts(models.Model):
    fullname = models.CharField(max_length=70, verbose_name='نام و نام خانوادگی')
    birthday = models.CharField(max_length=10, verbose_name='تاریخ تولد')
    phone = models.IntegerField(verbose_name='شماره تماس')
    sex = models.CharField(max_length=5, verbose_name='جنسیت')
    first_gift = models.CharField(max_length=60, default='', blank=True, verbose_name='جایزه اول')
    second_gift = models.CharField(max_length=60, default='', blank=True, verbose_name='جایزه دوم')
    created_at = models.DateTimeField(blank=True, null=True, verbose_name='تاریخ جایزه')

    def __str__(self):
        return self.fullname


class gitfsForm(ModelForm):
    class Meta:
        model = gifts
        fields = ('fullname', 'birthday', 'phone', 'sex')
        error_messages = {
            'fullname': {
                'required': "نام و نام خانوادگی الزامی است."
            },
            'birthday': {
                'required': "تاریخ تولد الزامی است."
            },
            'phone': {
                'required': "شمار تماس الزامی است."
            },
            'sex': {
                'required': "جنسیت نیاز است."
            },
        }
