# Generated by Django 2.1.4 on 2019-01-10 13:32

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('qorekeshi', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='gifts',
            name='created_at',
            field=models.DateTimeField(blank=True, null=True),
        ),
        migrations.AlterField(
            model_name='gifts',
            name='fullname',
            field=models.CharField(error_messages={'required': 'Please let us know what to call you!'}, max_length=70),
        ),
    ]
