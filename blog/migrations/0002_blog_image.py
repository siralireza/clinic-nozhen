# Generated by Django 2.1.4 on 2019-02-23 20:12

from django.db import migrations, models
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        ('blog', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='blog',
            name='image',
            field=models.ImageField(default=django.utils.timezone.now, upload_to='blog/%Y/%m/%d/'),
            preserve_default=False,
        ),
    ]
