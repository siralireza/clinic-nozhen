from django.shortcuts import render

# Create your views here.
from blog.models import blog


def blog_fa(request):
    return render(request, 'blog-fa.html', {'blog': blog.objects.all()})
