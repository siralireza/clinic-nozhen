from django.db import models


class blog(models.Model):
    title = models.CharField(max_length=70, verbose_name='عنوان')
    description = models.TextField(verbose_name='متن')
    created_at = models.DateTimeField(blank=True, null=True, verbose_name='تاریخ جایزه')
    image = models.ImageField()

    def __str__(self):
        return self.title
