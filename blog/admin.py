from django.contrib import admin

from blog.models import blog


@admin.register(blog)
class blogAdmin(admin.ModelAdmin):
    list_display = ['id', 'title', 'description']
